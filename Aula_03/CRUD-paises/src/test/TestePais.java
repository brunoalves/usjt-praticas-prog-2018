package test;

import static org.junit.Assert.assertEquals;
import model.Pais;
import org.junit.Before;
import org.junit.Test;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

import service.PaisService;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestePais {
	Pais pais, copiaComparar;
	PaisService paisService;
	static int id = 1;
	
	@Before
	public void setUp() throws Exception {
		System.out.println("setup");
		pais = new Pais();
		pais.setId(id);
		pais.setNome("Noruega");
		pais.setPopulacao(84815215);
		pais.setArea(12580.0);
		copiaComparar = new Pais();
		copiaComparar.setNome("Esc�cia");
		copiaComparar.setPopulacao(1475000);
		copiaComparar.setArea(81448.0);
		paisService = new PaisService();

		//outputs
		System.out.println(pais);
		System.out.println(copiaComparar);
		System.out.println(id);;
		
	}
	
	@Test
	public void testCarregar() {
		System.out.println("carregar");
		//para funcionar o cliente 1 deve ter sido carregado no banco por fora
		Pais fixture = new Pais();

		//fixture.setId(0);
		fixture.setNome("Carlos Drummond de Andrade");
		pais.setPopulacao(654321);
		pais.setArea(9999.0);
		PaisService novoService = new PaisService();
		Pais novo = novoService.carregar(pais);
		assertEquals("testa inclusao", novo.getId(), fixture.getId());
	}


	@Test
	public void testCriar() {
		System.out.println("Criar" + "\n-------------");
		id = paisService.criar(pais);
		System.out.println(id);
		copiaComparar.setId(id);
		assertEquals("testa criacao", pais.getId(), copiaComparar.getId());
	}
	
	@Test
	public void testAtualizar() {
		System.out.println("Atualizar" + "\n-------------");
		pais.setNome("Brasil atualizado");
		copiaComparar.setNome("C�pia atualizado");		
		paisService.atualizar(pais);
		pais = paisService.carregar(pais);
		assertEquals("testa atualizacao", pais.getNome(), copiaComparar.getNome());
	}
	
	@Test
	public void testExcluir() {
		System.out.println("Excluir" + "\n-------------");

		pais.setId(0);
		pais.setNome(null);
		pais.setPopulacao(0);
		pais.setArea(0);
		
		paisService.excluir(11);
		pais = paisService.carregar(pais);
		
		assertEquals("testa exclusao", pais.getId(), copiaComparar.getId());
	}
	
	@Test
	public void test04MaiorPop() {
		System.out.println("Maior populacao");
		paisService.maiorPopulacao(pais);
		long populacao = pais.getPopulacao();
		copiaComparar.setPopulacao(populacao);
		assertEquals("Testando maior populacao",pais.getPopulacao(),copiaComparar.getPopulacao());
}
	
	@Test
	public void test05MenorArea() {
		System.out.println("Menor Area");
		paisService.menorArea(pais);
		double area = pais.getArea();
		copiaComparar.setArea(area);
		assertEquals("Testando menor area",pais.getPopulacao(),copiaComparar.getPopulacao());
		
	
		
	}	
	@Test
	public void test06Array() {
		System.out.println("Vetor de 8 paises");
		String[] vet = pais.vetorTresPaises();
		System.out.println(vet);
		
		assertEquals("Testando Vetor de paises",vet.length,8);
	}

	
	




}